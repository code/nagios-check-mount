check\_mount
============

This plugin takes a mountpoint and checks that a filesystem is mounted
thereupon, once and once only.

Installation
------------

    $ perl Makefile.PL
    $ make
    $ sudo make install

Usage
-----

    Usage: check_mount --mountpoint|-m PATH
    
     -?, --usage
       Print usage information
     -h, --help
       Print detailed help screen
     -V, --version
       Print version information
     --extra-opts=[section][@file]
       Read options from an ini file. See https://www.monitoring-plugins.org/doc/extra-opts.html
       for usage and examples.
     -m, --mountpoint=PATH
       Path to mountpoint
     -t, --timeout=INTEGER
       Seconds before plugin times out (default: 15)
     -v, --verbose
       Show details for command-line debugging (can repeat up to 3 times)

Thanks
------

This was written on company time with my employer [Inspire Net][1], who has
generously allowed me to open source it.

License
-------

Copyright (c) [Tom Ryder][2]. Distributed under [MIT License][3].

[1]: https://www.inspire.net.nz/
[2]: https://sanctum.geek.nz/
[3]: https://opensource.org/licenses/MIT
